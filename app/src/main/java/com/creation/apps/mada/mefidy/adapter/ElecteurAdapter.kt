package com.creation.apps.mada.mefidy.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.creation.apps.mada.mefidy.R
import com.creation.apps.mada.mefidy.model.Electeur
import com.creation.apps.mada.mefidy.service.UtilService
import kotlinx.android.synthetic.main.list_item_cin.view.*

class ElecteurAdapter(val context : Context?, val electeurs : List<Electeur>) : RecyclerView.Adapter<ElecteurAdapter.ElectionViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ElectionViewHolder {
        val view =  LayoutInflater.from(context).inflate(R.layout.list_item_cin,parent,false)
        return ElectionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return electeurs.size
    }

    override fun onBindViewHolder(holder: ElectionViewHolder, position: Int) {
        val electeur = electeurs[position]
        holder.setData(electeur)
    }

    inner class ElectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun setData(electeur : Electeur?){
            itemView.nomPrenomText.text = electeur!!.nom +" "+electeur.prenom
            itemView.cinText.text = electeur.cin
            val bitmap=UtilService.getBitmapFromByte(electeur.image!!)
            itemView.photoItemImage.setImageBitmap(bitmap)
        }
    }
}