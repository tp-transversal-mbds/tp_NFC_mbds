package com.creation.apps.mada.mefidy.fragment

import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.creation.apps.mada.mefidy.R
import com.creation.apps.mada.mefidy.adapter.ElecteurAdapter
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.ProgressBar
import com.creation.apps.mada.mefidy.model.Electeur
import com.creation.apps.mada.mefidy.model.Utilisateur
import com.creation.apps.mada.mefidy.retrofitAPI.IElectionAPI
import com.creation.apps.mada.mefidy.retrofitAPI.RetrofitClient
import com.creation.apps.mada.mefidy.roomDao.ElecteurDatabase
import com.creation.apps.mada.mefidy.service.ElecteurService
import com.creation.apps.mada.mefidy.service.UtilService
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL

class ListCINFragment  : Fragment(){

    //-------------- Equivalent a http Session en web
    lateinit var session : SharedPreferences
    lateinit var gson : Gson
    lateinit var user : Utilisateur

    //-------------- consume Election API
    internal lateinit  var jsonApi: IElectionAPI
    //-------------- to make easy aync task
    internal var compositeDisposableElecteur = CompositeDisposable()

    //--------- database sqllite
    lateinit var electeurDatabase: ElecteurDatabase

    var allElecteur : List<Electeur> = ArrayList<Electeur>()

    var dataLoadingSpinner : ProgressBar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var currentView : View = inflater.inflate(R.layout.list_cin, container, false)

        session = PreferenceManager.getDefaultSharedPreferences(activity)
        gson = Gson()
        user = gson.fromJson(session.getString("userConnecter",""),Utilisateur::class.java)

        dataLoadingSpinner = currentView.findViewById(R.id.dataLoadingSpinner) as ProgressBar
        cacherSpinner()

        //-------------- affichage
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        val recyclerView = currentView.findViewById(R.id.recyclerViewCIN) as RecyclerView

        //----------- database initialisation
        electeurDatabase = ElecteurDatabase.getInstance(activity!!)!!
        allElecteur = ArrayList<Electeur>()

        //-------------- get all electeur by API
        val retrofit = RetrofitClient.instance
        jsonApi = retrofit.create(IElectionAPI::class.java)

        getAllElecteur(recyclerView, layoutManager)
//        fetchDataUsers(recyclerView,layoutManager)

        return currentView
    }

    //------------- traitement database access:
    //------------- si la base est vide on recupere les electeurs via le webAPI on insere tous dans la bdd
    //------------- sinon on recupere la liste dans la base local pour l'afficher

    //------------- on traite tous dans le callback car le traitement est asynchrone

    private fun getAllElecteur(recyclerView: RecyclerView, layoutManager: LinearLayoutManager){
        compositeDisposableElecteur.add(electeurDatabase.electeurDao().getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                afficherSpinner()
                Log.d("debog","call get all electeur db size: "+ it.size.toString())
                allElecteur = it
                Log.d("debog",allElecteur.size.toString())
                if(allElecteur.isEmpty()){
                    Log.d("debog","work true bloc vide zany")
                    fetchDataUsers(recyclerView, layoutManager)

                }else{
                    displayDataElecteurs(recyclerView, layoutManager,allElecteur)
                }
            }
        )
    }

    private fun insertElecteurs(){
        ElecteurService.insertAllElecteur(compositeDisposableElecteur,electeurDatabase,allElecteur)
        Log.d("debog","call insert all electeur db size: "+allElecteur.size.toString())
    }

    //---------------------------
    //----------- consume WEB API
    //---------------------------

    private fun fetchDataUsers(recyclerView: RecyclerView, layoutManager: LinearLayoutManager) {
        compositeDisposableElecteur.add(jsonApi.getElecteurByIdBureauVote(user.bv.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{jsonElecteur->

                allElecteur = ElecteurService.parseElecteurJson(jsonElecteur.data)
                doAsync {
                    var result =  getBitmapfromUrl(allElecteur)
                    uiThread {
                        result
                        Log.d("debog","call electeur web API: "+allElecteur.size.toString())
                        displayDataElecteurs(recyclerView,layoutManager,allElecteur)
                        Log.d("debog","userConnecter "+user.nom+" "+user.prenom)
                        insertElecteurs()
                    }
                }

            }
        )
    }

    private fun displayDataElecteurs(recyclerView: RecyclerView, layoutManager: LinearLayoutManager,electeurs : List<Electeur>) {
        val adapterElecteur= ElecteurAdapter(activity,electeurs)
        recyclerView.adapter = adapterElecteur
        recyclerView.layoutManager = layoutManager
        cacherSpinner()
    }

    private fun getBitmapfromUrl(electeurs : List<Electeur>){

        for(el in electeurs){
            val inputStream = URL(el.photoUrl).openStream()
            val bitmap = BitmapFactory.decodeStream(inputStream)
            el.image = UtilService.getPictureByteOfArray(bitmap!!)
        }

    }

    fun afficherSpinner(){
        dataLoadingSpinner!!.visibility = View.VISIBLE
    }

    fun cacherSpinner(){
        dataLoadingSpinner!!.visibility = View.INVISIBLE
    }


}