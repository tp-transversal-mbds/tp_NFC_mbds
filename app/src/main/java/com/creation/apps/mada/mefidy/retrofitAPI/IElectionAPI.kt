package com.creation.apps.mada.mefidy.retrofitAPI

import com.creation.apps.mada.mefidy.model.Electeur
import com.creation.apps.mada.mefidy.model.JsonElecteur
import com.creation.apps.mada.mefidy.model.JsonLogin
import com.creation.apps.mada.mefidy.model.Utilisateur
import io.reactivex.Observable
import retrofit2.http.*

interface IElectionAPI {

    //----------- GET values  = endpoint API
    @get:GET("persons/bv/423")
    val electeurs: Observable<JsonElecteur>

    //------------ GET get electeur by idBV
    @GET("persons/bv/{id}")
    fun getElecteurByIdBureauVote(@Path("id") id : Int) : Observable<JsonElecteur>

    //------------ POST Login
    @FormUrlEncoded
    @POST("utilisateurs/login")
    fun login(
        @Field("login") username : String,
        @Field("pass") password : String
    ):Observable<JsonLogin>
}