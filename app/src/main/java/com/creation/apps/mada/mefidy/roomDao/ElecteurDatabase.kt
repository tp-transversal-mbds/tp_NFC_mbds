package com.creation.apps.mada.mefidy.roomDao

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.creation.apps.mada.mefidy.model.Electeur

@Database(entities = arrayOf(Electeur::class), version = 1)
abstract class ElecteurDatabase : RoomDatabase() {
    abstract fun electeurDao(): ElecteurDao

    companion object {
        private var INSTANCE: ElecteurDatabase? = null

        fun getInstance(context: Context): ElecteurDatabase? {
            if (INSTANCE == null) {
                synchronized(ElecteurDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        ElecteurDatabase::class.java, "election.db")
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}