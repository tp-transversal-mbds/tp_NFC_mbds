package com.creation.apps.mada.mefidy.retrofitAPI

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

//    var baseUrl : String  = "http://192.168.43.223/angularResto/"
//    var baseUrl : String  = "http://192.168.1.111/angularResto/"
    var baseUrl : String  = "https://hifidy.herokuapp.com/api/"
    private var ourInstance: Retrofit?=null

    val instance:Retrofit
        get(){
            if(ourInstance ==null){
                ourInstance =Retrofit.Builder()
                    .baseUrl(this.baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            }
            return ourInstance!!
        }
}