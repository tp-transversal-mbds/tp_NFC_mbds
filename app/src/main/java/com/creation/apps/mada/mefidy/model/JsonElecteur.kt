package com.creation.apps.mada.mefidy.model

import com.google.gson.annotations.SerializedName

data class JsonElecteur(
    var data : List<ElecteurJSON>
)

data class ElecteurJSON (
    var cin: CIN,
    var bv: BureauVote,
    var _id: String,
    var nom: String,
    var prenom: String
)

data class CIN (
    var numero : String,
    var uid_nfc : String,
    var photo : String
)

data class BureauVote(
    var id : Int,
    @SerializedName("val")
    var name : String,
    var desc: String
)