package com.creation.apps.mada.mefidy.service

import android.content.Intent
import android.nfc.NdefMessage
import android.nfc.NfcAdapter

class NFCService{
    //--------- equivalent to static method in JAVA
    companion object {

        fun getNDefMessages(intent: Intent) : String {

            val rawMessage = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            val ndefMsg = rawMessage[0] as NdefMessage
            val ndefRecord = ndefMsg.records[0]
            var message = String(ndefRecord.payload)
            return message.substring(3)
        }

        fun getUID(intent: Intent) : String{
            val id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)
            return hexToString(id)
        }

        fun hexToString(hex : ByteArray) : String{
            var sb =  StringBuilder()
            for (b in hex) {
                sb.append(String.format("%02X", b))
            }
            return sb.toString()
        }
    }
}