package com.creation.apps.mada.mefidy.roomDao

import android.arch.persistence.room.*
import com.creation.apps.mada.mefidy.model.Electeur
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface ElecteurDao {
    @Query("SELECT * FROM Electeur")
    fun getAll(): Flowable<List<Electeur>>

    //---------- get Electeur by cin and uid
    @Query("SELECT * FROM Electeur WHERE cin = :cin AND uid = :uid LIMIT 1")
    fun findByCinAndUid(cin : String, uid: String): Maybe<Electeur>

    //---------- vararg  : Variable number of arguments (Varargs)
    //---------- allowing a variable number of arguments to be passed to the function:
    //---------- Exemple :  val list = myFunction(1, 2, 3)
    @Insert
    fun insertAll(vararg electeurs: Electeur)

    @Update
    fun updateAll(vararg electeurs: Electeur)

    @Delete
    fun delete(electeur: Electeur)
}
